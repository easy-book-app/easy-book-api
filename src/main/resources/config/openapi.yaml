---
openapi: "3.0.0"
info:
  title: "Easy Booking"
  description: "Gestor de usuarios y reservas para el negocio"
  version: "1.0.0"
servers:
  - url: "https://easybooking.com/v1/"
paths:

  /user:
    post:
      tags:
        - usuarios
      summary: Crea un nuevo usuario
      requestBody:
        description: "Información de usuario"
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/InputUser"
      responses:
        "400":
          description: "Información incorrecta"
        "201":
          description: "Usuario correctamente creado"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
    get:
      tags:
        - usuarios
      summary: Obtiene una lista de usuarios
      parameters:
        - in: "query"
          required: false
          name: "fullName"
          schema:
            type: string
      responses:
        "403":
          description: "No autorizado a obtener la lista de usuarios"
        "200":
          description: "Listado de usuarios, filtrado por nombre completo si es requerido."
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/User"
      security:
        - easyBookAuth: [admin]
  /user/login:
    post:
      tags:
        - usuarios
      summary: Autentica al usuario y devuelve un token para el uso de la api.
      requestBody:
          description: "Información de usuario"
          required: true
          content:
            application/json:
                schema:
                  type: object
                  properties:
                      dni:
                        type: string
                      password:
                        type: string
      responses:
        "403":
          description: "Login failed, invalid credentials"
        "200":
          description: "Token de autenticación"
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    type: string
  /user/{id}:
    put:
      tags:
        - usuarios
      summary: Edita un usuario propio o cualquiera si es el administrador.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
      requestBody:
        description: "Información de usuario"
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/InputUser"
      responses:
        "403":
          description: "No autorizado a editar este usuario"
        "200":
          description: "Usuario correctamente editado"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
      security:
        - easyBookAuth: [admin, easy]
    delete:
      tags:
        - usuarios
      summary: Elimina un usuario propio o cualquiera si es el administrador.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
      responses:
        "403":
          description: "No autorizado a editar este usuario"
        "204":
          description: "Usuario correctamente eliminado"
      security:
        - easyBookAuth: [admin, easy]
    get:
      tags:
        - usuarios
      summary: Obtiene un usuario propio o cualquiera si es el administrador.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
      responses:
        "403":
          description: "No autorizado a ver este usuario."
        "200":
          description: "Datos del usuario"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
      security:
        - easyBookAuth: [admin, easy]
  /user/{id}/book:
    post:
      tags:
        - reservas
      summary: Reserva una cita para un usuario. Solo podrán reservarse citas a futuro.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/InputBook"
      responses:
        "403":
          description: "No autorizado a crear una cita para este usuario."
        "400":
          description: "Fecha no disponible."
        "201":
          description: "Cita correctamente creada."
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Book"
      security:
        - easyBookAuth: [admin, easy]
    get:
      tags:
        - reservas
      summary: Listado de citas del usuario. Odenados de más recientes a más antiguas.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
        - in: "query"
          required: false
          name: "from"
          schema:
            type: string
            format: date-time
        - in: "query"
          required: false
          name: "to"
          schema:
            type: string
            format: date-time
      responses:
        "403":
          description: "No autorizado a crear una cita para este usuario."
        "200":
          description: "Listado de citas. Odenados de más recientes a más antiguas y filtrado en caso requerido."
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Book"
      security:
        - easyBookAuth: [admin, easy]
  /user/{id}/book/{bookId}:
    delete:
      tags:
        - reservas
      summary: Cancela una cita para un usuario. Solo podrán cancelarse las citas futuras.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
        - in: "path"
          required: true
          name: "bookId"
          description: El id del usuario
          schema:
            type: number
            format: int32
      responses:
        "403":
          description: "No autorizado a crear una cita para este usuario."
        "400":
          description: "Invalid BookId. Cita Incancelable"
        "204":
          description: "Cita correctamente cancelada."
      security:
        - easyBookAuth: [admin, easy]
    put:
      tags:
        - reservas
      summary: Edita una cita para un usuario. Solo podrán editarse las citas futuras.
      parameters:
        - in: "path"
          required: true
          name: "id"
          description: El id del usuario
          schema:
            type: string
            format: uuid
        - in: "path"
          required: true
          name: "bookId"
          description: El id del usuario
          schema:
            type: number
            format: int32
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/InputBook"
      responses:
        "403":
          description: "No autorizado a modificar una cita para este usuario."
        "400":
          description: "Fecha no disponible."
        "201":
          description: "Cita correctamente creada."
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Book"
      security:
        - easyBookAuth: [admin, easy]
  /book:
    get:
      tags:
        - reservas
        - administracion-reservas
      summary: Listado de todas las reservas disponibles.
      parameters:
        - in: "query"
          required: false
          name: "from"
          schema:
            type: string
            format: date-time
        - in: "query"
          required: false
          name: "to"
          schema:
            type: string
            format: date-time
      responses:
        "403":
          description: "No autorizado."
        "200":
          description: "Cita correctamente creada."
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/UserBook"
        "204":
          description: "Sin reservas para el periodo."
      security:
        - easyBookAuth: [admin]
  /book/config:
    get:
      tags:
        - reservas
        - administracion-reservas
      summary: Obtiene la configuración de reservas
      responses:
        "403":
          description: No autorizado a cambiar la configuración de reservas.
        "200":
          description: Configuración de reservas.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/BookConfig"
      security:
        - easyBookAuth: [admin]
    put:
      tags:
        - reservas
        - administracion-reservas
      summary: Edita la configuración de reservas
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/BookConfig"
      responses:
        "403":
          description: No autorizado a cambiar la configuración de reservas.
        "200":
          description: Configuración actualizada.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/BookConfig"
      security:
        - easyBookAuth: [admin]

  /book/opaque:
    get:
      tags:
        - reservas
      summary: Listado de fechas disponibles sin información de usuario.
      parameters:
        - in: "query"
          required: false
          name: "from"
          schema:
            type: string
            format: date
        - in: "query"
          required: false
          name: "to"
          schema:
            type: string
            format: date
      responses:
        "204":
          description: Sin fechas disponibles.
        "200":
          description: Fechas disponibles ordenadas de mas recientes a mas futuras.
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                  format: date-time
components:
  securitySchemes:
    easyBookAuth:
      type: "oauth2"
      flows:
        authorizationCode:
          authorizationUrl: "https://api.forwardkeys.com:6881/oauth2/code"
          tokenUrl: "https://api.forwardkeys.com:6882/oauth2/token"
          scopes:
            admin: "Administrators"
            easy: "Clients use."

  schemas:
    User:
      type: object
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
        surname:
          type: string
        dni:
          type: string
        role:
          type: string
          enum: [CLIENT, ADMIN]
        email:
          type: string
          format: email
    InputUser:
      type: object
      required: [name,surname,dni,email]
      properties:
        name:
          type: string
        surname:
          type: string
        dni:
          type: string
        email:
          type: string
          format: email
        password:
          type: string
    InputBook:
      type: object
      required: [date]
      properties:
        date:
          type: string
          format: date-time
    Book:
      type: object
      properties:
        id:
          type: integer
        date:
          type: string
          format: date-time
    UserBook:
      type: object
      properties:
        user:
          $ref: "#/components/schemas/User"
        book:
          $ref: "#/components/schemas/Book"
    BookConfig:
      type: object
      properties:
        slotTime:
          type: integer
          description: El tiempo de duracion de cada franja.
        availableWeekDay:
          $ref: "#/components/schemas/WeekDayAvailability"
        maxAvailableDate:
          type: string
          format: date
          description: El dia maximo con reservas abiertas
        specialDaysOff:
          description: Dias cerrados excepcionales. Si hubiera una cita en ese dia se cancelará automaticamente.
          type: array
          items:
            type: string
            format: date

    WeekDayAvailability:
      type: object
      description: Horario regular. Si hubiera alguna cita ya reservada se cancelara automaticamente.
      required: [monday, tuesday, wednesday, thursday, friday, saturday, sunday]
      properties:
        monday:
          $ref: "#/components/schemas/DayConfiguration"
        tuesday:
          $ref: "#/components/schemas/DayConfiguration"
        wednesday:
          $ref: "#/components/schemas/DayConfiguration"
        thursday:
          $ref: "#/components/schemas/DayConfiguration"
        friday:
          $ref: "#/components/schemas/DayConfiguration"
        saturday:
          $ref: "#/components/schemas/DayConfiguration"
        sunday:
          $ref: "#/components/schemas/DayConfiguration"
    DayConfiguration:
      type: object
      required: [active]
      properties:
        active:
          type: boolean
        start:
          type: string
          format: time
        end:
          type: string
          format: time
        pause:
          type: object
          properties:
            from:
              type: string
              format: time
            to:
              type: string
              format: time
