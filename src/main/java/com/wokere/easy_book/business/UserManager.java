package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.business.util.BusinessUtils;
import com.wokere.easy_book.entity.UserEntity;
import com.wokere.easy_book.integration.IDynamoDbIntegration;
import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.util.ExceptionUtil;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class UserManager implements IUserManager{

    private static final IDynamoDbIntegration dynamo = SingletonServiceFactory.getBean(IDynamoDbIntegration.class);

    @Override
    public UUID createUser(InputUser user) throws ApiException {
        UUID userId = UUID.nameUUIDFromBytes((user.getDni()+ LocalDateTime.now()).getBytes());
        dynamo.createUser(BusinessUtils.getUser(user, userId));
        return userId;
    }

    @Override
    public void updateUser(InputUser user, UUID userId) throws ApiException {
        User oldUser = dynamo.getUser(userId);
        BusinessUtils.dniHasNotChanged( oldUser, user);
        UserEntity userEntity = BusinessUtils.getUser(user, userId);
        dynamo.updateUser(userEntity);
    }

    @Override
    public User getUser(UUID id) throws ApiException {
        User user =  dynamo.getUser(id);
        if(user == null){
            throw ExceptionUtil.throwApiException("User not found", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    @Override
    public void deleteUser(UUID id) throws ApiException {
        dynamo.deleteUser(id);
        //TODO delete its bookings too.
    }

    @Override
    public List<User> getUsers(String nameFilter) throws ApiException {
        return dynamo.getUsers(nameFilter);
    }

    @Override
    public boolean userExists(UUID userId) throws ApiException {
        return dynamo.getUser(userId) != null;
    }
}
