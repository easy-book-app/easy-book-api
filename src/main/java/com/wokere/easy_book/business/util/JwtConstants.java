package com.wokere.easy_book.business.util;

public class JwtConstants {
    private JwtConstants() {
    }
    public static final String ROLE_CLAIM = "role";
    public static final String USER_CLAIM = "user";

}
