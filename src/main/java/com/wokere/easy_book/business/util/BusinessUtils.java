package com.wokere.easy_book.business.util;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.networknt.utility.HashUtil;
import com.wokere.easy_book.entity.UserEntity;
import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.model.User.RoleEnum;
import com.wokere.easy_book.util.ExceptionUtil;
import java.util.UUID;

public class BusinessUtils {
    private BusinessUtils() {
    }

    public static UserEntity getUser(InputUser user, UUID id) throws ApiException {
        try {
            if(user.getPassword() != null) {
                String passwordCrypt = HashUtil.generateStrongPasswordHash(user.getPassword());
                return new UserEntity(user, id, RoleEnum.CLIENT, passwordCrypt);
            }
            return new UserEntity(user, id, RoleEnum.CLIENT);
        }catch (Exception e) {
            throw  ExceptionUtil.throwApiException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    public static void dniHasNotChanged(User user, InputUser inputUser) throws ApiException {
        if(!user.getDni().equals(inputUser.getDni())) {
            throw ExceptionUtil.throwApiException("Dni can not be changed", HttpStatus.BAD_REQUEST);
        }
    }

}
