package com.wokere.easy_book.business.util;

import static com.wokere.easy_book.business.util.JwtConstants.ROLE_CLAIM;
import static com.wokere.easy_book.business.util.JwtConstants.USER_CLAIM;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.wokere.easy_book.model.JwtConfig;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.model.User.RoleEnum;
import com.wokere.easy_book.util.ExceptionUtil;
import com.wokere.easy_book.util.YamlUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public class JwtUtils {

    private static final int TOKEN_EXPIRATION_TIME_SECONDS = 3600;

    private static final ZoneOffset ZONE_OFFSET = ZoneId.systemDefault().getRules().getOffset(Instant.now());
    private static final JwtConfig config = YamlUtils.getJwtConfig();

    private JwtUtils() {
    }

    public static String generateToken(User user) {
        Date startDate = Date.from(LocalDateTime.now().toInstant(ZONE_OFFSET));
        Date endDate = Date.from(startDate.toInstant().plusSeconds(TOKEN_EXPIRATION_TIME_SECONDS));
        return Jwts.builder()
            .setIssuer(config.issuer())
            .setSubject(config.subject())
            .claim(ROLE_CLAIM, user.getRole())
            .claim(USER_CLAIM, user.getId())
            .setIssuedAt(startDate)
            .setNotBefore(startDate)
            .setExpiration(endDate)
            .signWith(Keys.hmacShaKeyFor(config.key().getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
            .compact();
    }

    public static Claims getClaims(String token) throws ApiException {
        try {
            Jws<Claims> a = Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(config.key().getBytes(StandardCharsets.UTF_8))).build().parseClaimsJws(token);
            return a.getBody();
        } catch (Exception e) {
            throw  ExceptionUtil.throwApiException("Invalid token", HttpStatus.UNAUTHORIZED);
        }
    }
}
