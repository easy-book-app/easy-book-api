package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.model.Token;

public interface IAuthenticationManager {
    Token authenticateUser(String user, String password) throws ApiException;
}
