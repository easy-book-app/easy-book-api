package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.model.User;
import java.util.List;
import java.util.UUID;

public interface IUserManager {
    UUID createUser(InputUser user) throws ApiException;
    void updateUser(InputUser user, UUID userId) throws ApiException;
    User getUser(UUID id) throws ApiException;
    void deleteUser(UUID id) throws ApiException;
    List<User> getUsers(String nameFilter) throws ApiException;
    boolean userExists(UUID userId) throws ApiException;
}
