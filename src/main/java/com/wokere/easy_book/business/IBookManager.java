package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.model.Book;
import com.wokere.easy_book.model.BookConfig;
import com.wokere.easy_book.model.DateFilter;
import com.wokere.easy_book.model.InputBook;
import com.wokere.easy_book.model.UserBook;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface IBookManager {

    int createBook(InputBook book, UUID userId) throws ApiException;
    void updateBook(InputBook book, UUID userId, int bookId);
    void deleteBook(int id, UUID userId);
    void deleteBooks(int userId);
    Book getBook(int id);
    List<Book> getBooks(DateFilter filter, UUID userId);
    List<UserBook> getBooks(DateFilter filter);
    List<LocalDateTime> getBookedDates(DateFilter filter);
    BookConfig getBookConfig();
    void updateBookConfig(BookConfig config);


}
