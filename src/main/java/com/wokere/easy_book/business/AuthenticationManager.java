package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.networknt.utility.HashUtil;
import com.wokere.easy_book.business.util.JwtUtils;
import com.wokere.easy_book.entity.UserCredentialEntity;
import com.wokere.easy_book.integration.IDynamoDbIntegration;
import com.wokere.easy_book.model.Token;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.util.ExceptionUtil;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class AuthenticationManager implements IAuthenticationManager {

    private static final IDynamoDbIntegration dynamo = SingletonServiceFactory.getBean(IDynamoDbIntegration.class);

    @Override
    public Token authenticateUser(String user, String password) throws ApiException {
        try {
            UserCredentialEntity creds = new UserCredentialEntity(user, password);
            UserCredentialEntity storedCreds = dynamo.getPassword(creds);
            boolean isPasswordCorrect = HashUtil.validatePassword(password.toCharArray(), storedCreds.password());
            if (!isPasswordCorrect) {
                throw ExceptionUtil.throwApiException("Invalid password", HttpStatus.UNAUTHORIZED);
            }
            User userModel = dynamo.getUser(storedCreds.uuid());
            return new Token(JwtUtils.generateToken(userModel));

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw ExceptionUtil.throwApiException("Error validating user", HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}