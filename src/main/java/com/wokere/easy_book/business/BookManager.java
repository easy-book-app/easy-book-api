package com.wokere.easy_book.business;

import com.networknt.exception.ApiException;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.integration.ICalendarIntegration;
import com.wokere.easy_book.model.Book;
import com.wokere.easy_book.model.BookConfig;
import com.wokere.easy_book.model.DateFilter;
import com.wokere.easy_book.model.InputBook;
import com.wokere.easy_book.model.UserBook;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class BookManager implements IBookManager{

    private static final ICalendarIntegration integration = SingletonServiceFactory.getBean(ICalendarIntegration.class);

    @Override
    public int createBook(InputBook book, UUID userId) throws ApiException {
         return 0;
    }

    @Override
    public void updateBook(InputBook book, UUID userId, int bookId) {
    }

    @Override
    public void deleteBook(int id, UUID userId) {
    }

    @Override
    public void deleteBooks(int userId) {
    }

    @Override
    public Book getBook(int id) {
        return null;
    }

    @Override
    public List<Book> getBooks(DateFilter filter, UUID userId) {
        return List.of();
    }

    @Override
    public List<UserBook> getBooks(DateFilter filter) {
        return List.of();
    }

    @Override
    public List<LocalDateTime> getBookedDates(DateFilter filter) {
        return List.of();
    }

    @Override
    public BookConfig getBookConfig() {
        return null;
    }

    @Override
    public void updateBookConfig(BookConfig config) {
    }
}
