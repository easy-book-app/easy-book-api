package com.wokere.easy_book.model;

public record JwtConfig(String key, String issuer, String subject) {

}
