package com.wokere.easy_book.model;

import java.time.LocalDateTime;

public record DateFilter(LocalDateTime from, LocalDateTime to) {

}
