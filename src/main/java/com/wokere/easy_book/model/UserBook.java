package com.wokere.easy_book.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserBook  {

    private User user;
    private Book book;


    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty("book")
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserBook userBook = (UserBook) o;

        return Objects.equals(user, userBook.user) &&
               Objects.equals(book, userBook.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UserBook {\n");
        sb.append("    user: ").append(toIndentedString(user)).append("\n");        sb.append("    book: ").append(toIndentedString(book)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
