package com.wokere.easy_book.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookConfig  {

    private Integer slotTime;
    private WeekDayAvailability availableWeekDay;
    private java.time.LocalDate maxAvailableDate;
    private java.util.List<String> specialDaysOff;


    @JsonProperty("slotTime")
    public Integer getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(Integer slotTime) {
        this.slotTime = slotTime;
    }

    @JsonProperty("availableWeekDay")
    public WeekDayAvailability getAvailableWeekDay() {
        return availableWeekDay;
    }

    public void setAvailableWeekDay(WeekDayAvailability availableWeekDay) {
        this.availableWeekDay = availableWeekDay;
    }

    @JsonProperty("maxAvailableDate")
    public java.time.LocalDate getMaxAvailableDate() {
        return maxAvailableDate;
    }

    public void setMaxAvailableDate(java.time.LocalDate maxAvailableDate) {
        this.maxAvailableDate = maxAvailableDate;
    }

    @JsonProperty("specialDaysOff")
    public java.util.List<String> getSpecialDaysOff() {
        return specialDaysOff;
    }

    public void setSpecialDaysOff(java.util.List<String> specialDaysOff) {
        this.specialDaysOff = specialDaysOff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookConfig bookConfig = (BookConfig) o;

        return Objects.equals(slotTime, bookConfig.slotTime) &&
               Objects.equals(availableWeekDay, bookConfig.availableWeekDay) &&
               Objects.equals(maxAvailableDate, bookConfig.maxAvailableDate) &&
               Objects.equals(specialDaysOff, bookConfig.specialDaysOff);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slotTime, availableWeekDay, maxAvailableDate, specialDaysOff);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class BookConfig {\n");
        sb.append("    slotTime: ").append(toIndentedString(slotTime)).append("\n");        sb.append("    availableWeekDay: ").append(toIndentedString(availableWeekDay)).append("\n");        sb.append("    maxAvailableDate: ").append(toIndentedString(maxAvailableDate)).append("\n");        sb.append("    specialDaysOff: ").append(toIndentedString(specialDaysOff)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
