package com.wokere.easy_book.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DayConfiguration  {

    private Boolean active;
    private String start;
    private String end;
    private Object pause;

    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    @JsonProperty("end")
    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @JsonProperty("pause")
    public Object getPause() {
        return pause;
    }

    public void setPause(Object pause) {
        this.pause = pause;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DayConfiguration dayConfiguration = (DayConfiguration) o;

        return Objects.equals(active, dayConfiguration.active) &&
               Objects.equals(start, dayConfiguration.start) &&
               Objects.equals(end, dayConfiguration.end) &&
               Objects.equals(pause, dayConfiguration.pause);
    }

    @Override
    public int hashCode() {
        return Objects.hash(active, start, end, pause);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DayConfiguration {\n");
        sb.append("    active: ").append(toIndentedString(active)).append("\n");        sb.append("    start: ").append(toIndentedString(start)).append("\n");        sb.append("    end: ").append(toIndentedString(end)).append("\n");        sb.append("    pause: ").append(toIndentedString(pause)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
