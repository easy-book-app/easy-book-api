package com.wokere.easy_book.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WeekDayAvailability  {

    private DayConfiguration monday;
    private DayConfiguration tuesday;
    private DayConfiguration wednesday;
    private DayConfiguration thursday;
    private DayConfiguration friday;
    private DayConfiguration saturday;
    private DayConfiguration sunday;


    @JsonProperty("monday")
    public DayConfiguration getMonday() {
        return monday;
    }

    public void setMonday(DayConfiguration monday) {
        this.monday = monday;
    }

    @JsonProperty("tuesday")
    public DayConfiguration getTuesday() {
        return tuesday;
    }

    public void setTuesday(DayConfiguration tuesday) {
        this.tuesday = tuesday;
    }

    @JsonProperty("wednesday")
    public DayConfiguration getWednesday() {
        return wednesday;
    }

    public void setWednesday(DayConfiguration wednesday) {
        this.wednesday = wednesday;
    }

    @JsonProperty("thursday")
    public DayConfiguration getThursday() {
        return thursday;
    }

    public void setThursday(DayConfiguration thursday) {
        this.thursday = thursday;
    }

    @JsonProperty("friday")
    public DayConfiguration getFriday() {
        return friday;
    }

    public void setFriday(DayConfiguration friday) {
        this.friday = friday;
    }

    @JsonProperty("saturday")
    public DayConfiguration getSaturday() {
        return saturday;
    }

    public void setSaturday(DayConfiguration saturday) {
        this.saturday = saturday;
    }

    @JsonProperty("sunday")
    public DayConfiguration getSunday() {
        return sunday;
    }

    public void setSunday(DayConfiguration sunday) {
        this.sunday = sunday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WeekDayAvailability weekDayAvailability = (WeekDayAvailability) o;

        return Objects.equals(monday, weekDayAvailability.monday) &&
               Objects.equals(tuesday, weekDayAvailability.tuesday) &&
               Objects.equals(wednesday, weekDayAvailability.wednesday) &&
               Objects.equals(thursday, weekDayAvailability.thursday) &&
               Objects.equals(friday, weekDayAvailability.friday) &&
               Objects.equals(saturday, weekDayAvailability.saturday) &&
               Objects.equals(sunday, weekDayAvailability.sunday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class WeekDayAvailability {\n");
        sb.append("    monday: ").append(toIndentedString(monday)).append("\n");        sb.append("    tuesday: ").append(toIndentedString(tuesday)).append("\n");        sb.append("    wednesday: ").append(toIndentedString(wednesday)).append("\n");        sb.append("    thursday: ").append(toIndentedString(thursday)).append("\n");        sb.append("    friday: ").append(toIndentedString(friday)).append("\n");        sb.append("    saturday: ").append(toIndentedString(saturday)).append("\n");        sb.append("    sunday: ").append(toIndentedString(sunday)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
