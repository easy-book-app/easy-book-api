package com.wokere.easy_book.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InputUser  {

    private String name;
    private String surname;
    private String dni;
    private String email;
    private String password;


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InputUser inputUser = (InputUser) o;

        return Objects.equals(name, inputUser.name) &&
               Objects.equals(surname, inputUser.surname) &&
               Objects.equals(dni, inputUser.dni)
               && Objects.equals(email, inputUser.email)
               && Objects.equals(password, inputUser.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, dni, email, password);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class InputUser {\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");        sb.append("    surname: ").append(toIndentedString(surname)).append("\n");        sb.append("    dni: ").append(toIndentedString(dni)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
