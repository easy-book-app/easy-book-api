package com.wokere.easy_book.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InputBook  {

    private java.time.LocalDateTime date;

    @JsonProperty("date")
    public java.time.LocalDateTime getDate() {
        return date;
    }

    public void setDate(java.time.LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InputBook inputBook = (InputBook) o;

        return Objects.equals(date, inputBook.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class InputBook {\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
