package com.wokere.easy_book.handler;

import com.networknt.body.BodyHandler;
import com.networknt.config.Config;

import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IBookController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import com.wokere.easy_book.model.InputBook;
import java.util.Map;
import java.util.UUID;


public class UserIdBookPostHandler implements LightHttpHandler {
    private static final IBookController controller = SingletonServiceFactory.getBean(IBookController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        UUID userId = UUID.fromString(exchange.getQueryParameters().get("id").getFirst());
        @SuppressWarnings("unchecked") Map<String, Object> bodyMap = (Map<String, Object>)exchange.getAttachment(BodyHandler.REQUEST_BODY);
        InputBook requestBody = Config.getInstance().getMapper().convertValue(bodyMap, InputBook.class);
        SendUtils.send(exchange, controller.createBook(requestBody, userId), HttpStatus.CREATED);
    }
}
