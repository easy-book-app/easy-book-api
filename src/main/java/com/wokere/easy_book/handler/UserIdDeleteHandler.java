package com.wokere.easy_book.handler;

import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IUserController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import java.util.UUID;

public class UserIdDeleteHandler implements LightHttpHandler {

    private static final IUserController controller = SingletonServiceFactory.getBean(IUserController.class);
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        UUID userId = UUID.fromString(exchange.getQueryParameters().get("id").getFirst());
        controller.deleteUser(userId);
        SendUtils.send(exchange);
    }
}
