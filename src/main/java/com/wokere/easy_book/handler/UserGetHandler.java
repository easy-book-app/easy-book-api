package com.wokere.easy_book.handler;



import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IUserController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;


public class UserGetHandler implements LightHttpHandler {
    private static final IUserController controller = SingletonServiceFactory.getBean(IUserController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        SendUtils.send(exchange,controller.getUsers(exchange.getQueryParameters()), HttpStatus.OK);

    }
}
