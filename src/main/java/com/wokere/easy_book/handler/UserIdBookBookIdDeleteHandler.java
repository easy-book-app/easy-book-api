package com.wokere.easy_book.handler;

import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IBookController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import java.util.UUID;

public class UserIdBookBookIdDeleteHandler implements LightHttpHandler {

    private static final IBookController controller = SingletonServiceFactory.getBean(IBookController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        UUID userId = UUID.fromString(exchange.getQueryParameters().get("userId").getFirst());
        int bookId = Integer.parseInt(exchange.getQueryParameters().get("bookId").getFirst());
        controller.deleteBook(bookId, userId);
        SendUtils.send(exchange);
    }
}
