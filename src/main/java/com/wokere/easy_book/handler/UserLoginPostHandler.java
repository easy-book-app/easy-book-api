package com.wokere.easy_book.handler;

import com.networknt.body.BodyHandler;
import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IUserController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import java.util.Map;

public class UserLoginPostHandler implements LightHttpHandler {
    private static final IUserController controller = SingletonServiceFactory.getBean(IUserController.class);
    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        @SuppressWarnings("unchecked") Map<String, Object> bodyMap = (Map<String, Object>)httpServerExchange.getAttachment(BodyHandler.REQUEST_BODY);
        SendUtils.send(httpServerExchange,controller.authenticateUser(bodyMap), HttpStatus.OK);
    }
}
