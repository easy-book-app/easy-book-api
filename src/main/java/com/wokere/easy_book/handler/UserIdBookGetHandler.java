package com.wokere.easy_book.handler;

import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IBookController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import java.util.UUID;

public class UserIdBookGetHandler implements LightHttpHandler {
    private static final IBookController controller = SingletonServiceFactory.getBean(IBookController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        UUID userId = UUID.fromString(exchange.getQueryParameters().get("id").getFirst());
        SendUtils.send(exchange, controller.getBooks(exchange.getQueryParameters(), userId), HttpStatus.OK);
    }
}
