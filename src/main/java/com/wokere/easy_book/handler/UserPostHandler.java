package com.wokere.easy_book.handler;

import com.networknt.body.BodyHandler;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IUserController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;
import com.wokere.easy_book.model.InputUser;
import java.util.Map;

public class UserPostHandler implements LightHttpHandler {

    private static final IUserController controller = SingletonServiceFactory.getBean(IUserController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        @SuppressWarnings("unchecked") Map<String, Object> bodyMap = (Map<String, Object>) exchange.getAttachment(BodyHandler.REQUEST_BODY);
        InputUser requestBody = Config.getInstance().getMapper().convertValue(bodyMap, InputUser.class);
        SendUtils.send(exchange, controller.createUser(requestBody), HttpStatus.CREATED);

    }
}
