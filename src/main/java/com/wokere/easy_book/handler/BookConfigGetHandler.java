package com.wokere.easy_book.handler;


import com.networknt.handler.LightHttpHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.controller.IBookController;
import com.wokere.easy_book.util.SendUtils;
import io.undertow.server.HttpServerExchange;

public class BookConfigGetHandler implements LightHttpHandler {

    private static final IBookController controller = SingletonServiceFactory.getBean(IBookController.class);

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        SendUtils.send(exchange, controller.getBookConfig(), HttpStatus.OK);
    }
}
