package com.wokere.easy_book.integration.util;

import com.wokere.easy_book.entity.UserEntity;
import com.wokere.easy_book.model.User;
import java.util.HashMap;
import java.util.Map;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

public class ModelIntegrationUtil {

    private ModelIntegrationUtil() {
    }
    public static final String ID = "id";
    public static final String NAME = "name";
    public static  final String SURNAME = "surname";
    public static final String ROLE = "role";
    public static final String DNI = "dni";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";


    public static Map<String, AttributeValue> toMap(UserEntity user){
        Map<String,AttributeValue> map = new HashMap<>();
        map.put(ID, AttributeValue.builder().s(user.id().toString()).build());
        map.put(NAME, AttributeValue.builder().s(user.name()).build());
        map.put(SURNAME, AttributeValue.builder().s(user.surname()).build());
        map.put(ROLE, AttributeValue.builder().s(user.role().toString()).build());
        map.put(DNI, AttributeValue.builder().s(user.dni()).build());
        map.put(EMAIL, AttributeValue.builder().s(user.email()).build());
        if (user.password() != null) {
            map.put(PASSWORD, AttributeValue.builder().s(user.password()).build());
        }
        return map;
    }

    public static User toUser(Map<String, AttributeValue> userMap){
        User user = new User();
        user.setId(java.util.UUID.fromString(userMap.get(ID).s()));
        user.setName(userMap.get(NAME).s());
        user.setSurname(userMap.get(SURNAME).s());
        user.setRole(User.RoleEnum.fromValue(userMap.get(ROLE).s()));
        user.setDni(userMap.get(DNI).s());
        user.setEmail(userMap.get(EMAIL).s());
        return user;
    }
}

