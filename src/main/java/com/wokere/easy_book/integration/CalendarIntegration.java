package com.wokere.easy_book.integration;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.wokere.easy_book.entity.CalendarConfig;
import com.wokere.easy_book.util.ExceptionUtil;
import com.wokere.easy_book.util.YamlUtils;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDateTime;

public class CalendarIntegration implements ICalendarIntegration {

    private static final CalendarConfig config = YamlUtils.getcalendarConfig();
    private static final String DETAILS = String.format("Appointment %s", config.name());

    @Override
    public String addEvent(LocalDateTime start, LocalDateTime end) throws ApiException {
        try {
            Calendar service = getClient();
            Event event = getEvent(start, end);
            Event response = service.events().insert(config.id(), event).execute();
            return response.getId();
        } catch (Exception e) {
            throw ExceptionUtil.throwApiException("Error while adding event to google calendar", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void updateEvent(LocalDateTime start, LocalDateTime ends, String id) throws ApiException {
        try {
            Calendar service = getClient();
            Event event = getEvent(start, ends);
            service.events().update(config.id(), id, event).execute();
        } catch (Exception e) {
            throw ExceptionUtil.throwApiException("Error while updating event to google calendar", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public void deleteEvent(String id) throws ApiException {
        try {
            Calendar service = getClient();
            service.events().delete(config.id(), id).execute();
        } catch (Exception e) {
            throw ExceptionUtil.throwApiException("Error while deleting event to google calendar", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private HttpRequestInitializer getRequestInitializer() {
        return request ->
            request.setInterceptor(interceptedRequest -> interceptedRequest.getUrl().set("key", config.key()));
    }

    private Calendar getClient() throws GeneralSecurityException, IOException {
        return new Calendar.Builder(GoogleNetHttpTransport.newTrustedTransport(), GsonFactory.getDefaultInstance(), getRequestInitializer()).setApplicationName("easy-book")
            .build();
    }

    private Event getEvent(LocalDateTime start, LocalDateTime end) {
        EventDateTime startDateTime = new EventDateTime()
            .setDateTime(new com.google.api.client.util.DateTime(start.toString()));
        EventDateTime endDateTime = new EventDateTime()
            .setDateTime(new com.google.api.client.util.DateTime(end.toString()));
        return new Event()
            .setSummary(DETAILS)
            .setDescription(DETAILS)
            .setStart(startDateTime)
            .setEnd(endDateTime);
    }
}
