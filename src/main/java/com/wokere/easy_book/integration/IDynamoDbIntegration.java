package com.wokere.easy_book.integration;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.entity.UserCredentialEntity;
import com.wokere.easy_book.entity.UserEntity;
import com.wokere.easy_book.model.User;
import java.util.List;
import java.util.UUID;

public interface IDynamoDbIntegration {
    void createUser(UserEntity user) throws ApiException;
    void updateUser(UserEntity user) throws ApiException;
    User getUser(UUID id) throws ApiException;
    void deleteUser(UUID id) throws ApiException;

    List<User> getUsers(String nameFilter) throws ApiException;
    UserCredentialEntity getPassword(UserCredentialEntity userCredential) throws ApiException;
}
