package com.wokere.easy_book.integration;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.wokere.easy_book.entity.DynamoConfig;
import com.wokere.easy_book.entity.UserCredentialEntity;
import com.wokere.easy_book.entity.UserEntity;
import com.wokere.easy_book.integration.util.ModelIntegrationUtil;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.util.ExceptionUtil;
import com.wokere.easy_book.util.YamlUtils;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.DynamoDbClientBuilder;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemResponse;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ReturnValue;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;

public class DynamoDbIntegration implements IDynamoDbIntegration{

    private static final DynamoConfig config = YamlUtils.getDynamoConfig();
    private static final AwsBasicCredentials CREDENTIALS = AwsBasicCredentials.create(config.key(), config.secret());
    private static final DynamoDbClientBuilder BUILDER = DynamoDbClient.builder().credentialsProvider(() -> CREDENTIALS).region(Region.of(config.region()));


    @Override
    public void createUser(UserEntity user) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            Map<String,AttributeValue> userMap = ModelIntegrationUtil.toMap(user);
            PutItemRequest request = PutItemRequest.builder()
                .tableName(config.userTable())
                .returnValues(ReturnValue.ALL_OLD)
                .item(userMap)
                .build();
             client.putItem(request);
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to create user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void updateUser(UserEntity user) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            Map<String,AttributeValue> userMap = ModelIntegrationUtil.toMap(user);
            PutItemRequest request = PutItemRequest.builder()
                .tableName(config.userTable())
                .item(userMap)
                .build();
            client.putItem(request);
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to update user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public User getUser(UUID id) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            GetItemRequest request = GetItemRequest.builder()
                .tableName(config.userTable())
                .key(Map.of(ModelIntegrationUtil.ID, AttributeValue.builder().s(id.toString()).build()))
                .build();
            GetItemResponse response = client.getItem(request);
            return response.item().isEmpty() ? null: ModelIntegrationUtil.toUser(response.item());
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to retrieve the user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void deleteUser(UUID id) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            DeleteItemRequest request = DeleteItemRequest.builder()
                .tableName(config.userTable())
                .key(Map.of(ModelIntegrationUtil.ID, AttributeValue.builder().s(id.toString()).build()))
                .build();
            DeleteItemResponse response = client.deleteItem(request);
            response.attributes();
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to delete user", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<User> getUsers(String nameFilter) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            ScanRequest.builder().tableName(config.userTable()).build();
            ScanRequest request = ScanRequest.builder()
                .tableName(config.userTable())
                .filterExpression("begins_with(#surname, :filter) or begins_with(#name, :filter)")
                .expressionAttributeNames(Map.of("#surname", "surname", "#name", "name"))
                .expressionAttributeValues(Map.of(":filter", AttributeValue.builder().s(nameFilter).build()))
                .build();
            ScanResponse response = client.scan(request);
            return response.items().stream().map(ModelIntegrationUtil::toUser).toList();
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to get users", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public UserCredentialEntity getPassword(UserCredentialEntity userCredential) throws ApiException {
        try(DynamoDbClient client = BUILDER.build()){
            ScanRequest.builder().tableName(config.userTable()).build();
            ScanRequest request = ScanRequest.builder()
                .tableName(config.userTable())
                .filterExpression("#dni = :dni")
                .expressionAttributeNames(Map.of("#dni", "dni" ))
                .expressionAttributeValues(Map.of(":dni", AttributeValue.builder().s(userCredential.dni()).build() ))
                .build();
            ScanResponse response = client.scan(request);
            return response.items().stream().map(r -> new UserCredentialEntity(UUID.fromString(r.get(ModelIntegrationUtil.ID).s()),r.get(ModelIntegrationUtil.DNI).s(),r.get(ModelIntegrationUtil.PASSWORD).s())).findFirst().orElseThrow();
        }catch (Exception e){
            throw ExceptionUtil.throwApiException("Failed to get credentials", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
