package com.wokere.easy_book.integration;

import com.networknt.exception.ApiException;
import java.time.LocalDateTime;

public interface ICalendarIntegration {

    String addEvent(LocalDateTime start, LocalDateTime ends) throws ApiException;
    void updateEvent(LocalDateTime start, LocalDateTime ends, String id) throws ApiException;
    void deleteEvent(String id) throws ApiException;
}
