package com.wokere.easy_book.middleware.util;

import io.jsonwebtoken.Claims;
import io.undertow.util.AttachmentKey;

public class Constants {
    private Constants() {
    }

    public static final AttachmentKey<Claims> CLAIMS_ATTACHMENT_KEY = AttachmentKey.create(Claims.class);
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
}
