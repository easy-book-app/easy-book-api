package com.wokere.easy_book.middleware;

import com.networknt.handler.Handler;
import com.networknt.handler.MiddlewareHandler;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.business.IUserManager;
import com.wokere.easy_book.business.util.JwtConstants;
import com.wokere.easy_book.middleware.util.Constants;
import com.wokere.easy_book.model.User;
import com.wokere.easy_book.model.User.RoleEnum;
import com.wokere.easy_book.util.ExceptionUtil;
import io.jsonwebtoken.Claims;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

public class UserValidation implements MiddlewareHandler {

    private HttpHandler next;

    @Override
    public HttpHandler getNext() {
        return this.next;
    }

    @Override
    public MiddlewareHandler setNext(HttpHandler next) {
        Handlers.handlerNotNull(next);
        this.next = next;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void register() {
        // Unused but required by the interface
    }

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        Claims claims = httpServerExchange.getAttachment(Constants.CLAIMS_ATTACHMENT_KEY);
        UUID userId = UUID.fromString(claims.get(JwtConstants.USER_CLAIM, String.class));
        RoleEnum role = RoleEnum.valueOf(claims.get(JwtConstants.ROLE_CLAIM, String.class));
        if (role != RoleEnum.ADMIN) {
            try {
                Optional.of(Arrays.asList(httpServerExchange.getRequestPath().split("/")))
                    .filter(p -> p.size() >= 4)
                    .map(p -> p.get(3))
                    .filter(id -> id.equals(userId.toString()))
                    .orElseThrow();
            } catch (Exception e) {
                throw ExceptionUtil.throwApiException("Unauthorize to perform the operation", HttpStatus.BAD_REQUEST);
            }
        }
        Handler.next(httpServerExchange, this.next);
    }
}
