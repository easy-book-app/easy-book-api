package com.wokere.easy_book.middleware;

import com.networknt.handler.Handler;
import com.networknt.handler.MiddlewareHandler;
import com.networknt.http.HttpStatus;
import com.wokere.easy_book.business.util.JwtConstants;
import com.wokere.easy_book.business.util.JwtUtils;
import com.wokere.easy_book.middleware.util.Constants;
import com.wokere.easy_book.model.JwtConfig;
import com.wokere.easy_book.util.ExceptionUtil;
import com.wokere.easy_book.util.YamlUtils;
import io.jsonwebtoken.Claims;
import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class TokenValidation implements MiddlewareHandler {
    private static final JwtConfig jwtConfig = YamlUtils.getJwtConfig();
    private HttpHandler next;


    @Override
    public HttpHandler getNext() {
        return this.next;
    }

    @Override
    public MiddlewareHandler setNext(HttpHandler next) {
        Handlers.handlerNotNull(next);
        this.next = next;
        return this;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void register() {
        //Unused but required by the interface
    }

    @Override
    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        boolean isAuthPresent = httpServerExchange.getRequestHeaders().contains(Constants.AUTHORIZATION_HEADER_NAME) && httpServerExchange.getRequestHeaders().get("Authorization").getFirst().startsWith("Bearer ");
        if(!isAuthPresent) {
            throw ExceptionUtil.throwApiException("Authorization header not present", HttpStatus.BAD_REQUEST);
        }
        String token = httpServerExchange.getRequestHeaders().get(Constants.AUTHORIZATION_HEADER_NAME).getFirst().split(" ")[1];
        Claims claims = JwtUtils.getClaims(token);
        boolean isTokenValid = claims.containsKey(JwtConstants.ROLE_CLAIM) && claims.containsKey(JwtConstants.USER_CLAIM) && claims.getIssuer().equals(jwtConfig.issuer());
        if(!isTokenValid) {
            throw ExceptionUtil.throwApiException("Invalid token", HttpStatus.UNAUTHORIZED);
        }
        httpServerExchange.putAttachment(Constants.CLAIMS_ATTACHMENT_KEY, claims);
        Handler.next(httpServerExchange, this.next);


    }
}
