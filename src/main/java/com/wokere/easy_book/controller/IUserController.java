package com.wokere.easy_book.controller;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.model.InputUser;
import java.util.Deque;
import java.util.Map;
import java.util.UUID;

public interface IUserController {

    String createUser(InputUser user) throws ApiException;

    String updateUser(InputUser user, UUID userId) throws ApiException;

    String getUser(UUID id) throws ApiException;

    void deleteUser(UUID id) throws ApiException;

    String getUsers(Map<String, Deque<String>> queryParameters) throws ApiException;

    String authenticateUser(Map<String, Object> bodyMap) throws ApiException;

}
