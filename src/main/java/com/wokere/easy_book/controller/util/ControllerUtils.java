package com.wokere.easy_book.controller.util;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.business.IUserManager;
import com.wokere.easy_book.model.DateFilter;
import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.util.ExceptionUtil;
import java.time.LocalDateTime;
import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;

public class ControllerUtils {

    private static final IUserManager userManager = SingletonServiceFactory.getBean(IUserManager.class);

    private ControllerUtils() {
    }

    public static String  getNameFilter(Map<String, Deque<String>> queryParameters) {
        return StringUtils.capitalize(Optional.ofNullable(queryParameters.get("fullName"))
                .map(Deque::getFirst)
                .orElse(StringUtils.EMPTY));
    }

    public static void validateUser(UUID userId) throws ApiException {
        if (!userManager.userExists(userId)) {
            throw ExceptionUtil.throwApiException("User not exist", HttpStatus.NOT_FOUND);
        }
    }

    public static void validateUserCreation(InputUser user) throws ApiException {
        boolean isDniValid = isDniValid(user.getDni()) || isNieValid(user.getDni());
        if(isDniValid || user.getName() == null || user.getSurname() == null || user.getEmail() == null || user.getPassword() == null) {
            throw ExceptionUtil.throwApiException("Missing required fields", HttpStatus.BAD_REQUEST);
        }
    }


    public static DateFilter getDateFilter(Map<String, Deque<String>> queryParameters) {
        LocalDateTime fromDate = Optional.ofNullable(queryParameters.get("fromDate"))
                .map(Deque::getFirst)
                .map(LocalDateTime::parse)
                .orElse(null);
        LocalDateTime toDate = Optional.ofNullable(queryParameters.get("toDate"))
            .map(Deque::getFirst)
            .map(LocalDateTime::parse)
            .orElse(null);
        return new DateFilter(fromDate, toDate);
    }

    private static boolean isDniValid(String dni) {
        try {
            int dniNumbers = Integer.parseInt(dni.substring(0, 8));
            char dniLetter = dni.charAt(8);
            String[] chars = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};
            int position = dniNumbers % 23;
            return chars[position].equals(String.valueOf(dniLetter));
        }catch (Exception e) {
            return false;
        }
    }

    private static boolean isNieValid(String nie){
        try {
            String nieLetter = nie.substring(0, 1);
            String nieNumbers = nie.substring(1, 8);
            String[] chars = {"X", "Y", "Z"};
            int position = Integer.parseInt(nieNumbers) % 23;
            boolean isInitialLetterOk = chars[position].equals(nieLetter);
            if(!isInitialLetterOk){
                return false;
            }
            String toDni = position+ nieNumbers;
            return isDniValid(toDni);
        }catch (Exception e) {
            return false;
        }
    }

}
