package com.wokere.easy_book.controller;

import com.networknt.exception.ApiException;
import com.wokere.easy_book.model.BookConfig;
import com.wokere.easy_book.model.InputBook;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Deque;
import java.util.Map;
import java.util.UUID;

public interface IBookController {
    String createBook(InputBook book, UUID userId) throws ApiException, GeneralSecurityException, IOException;
    String updateBook(InputBook book, UUID userId, int bookId) throws ApiException;
    void deleteBook(int id, UUID userId) throws ApiException;
    String getBooks(Map<String, Deque<String>> queryParameters, UUID userId) throws ApiException;
    String getAllUserBooks(Map<String, Deque<String>> queryParameters) throws ApiException;
    String getBookedDates(Map<String, Deque<String>> queryParameters) throws ApiException;
    String getBookConfig();
    String updateBookConfig(BookConfig body);


}
