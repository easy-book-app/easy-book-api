package com.wokere.easy_book.controller;

import com.networknt.config.Config;
import com.networknt.exception.ApiException;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.business.IAuthenticationManager;
import com.wokere.easy_book.business.IUserManager;
import com.wokere.easy_book.controller.util.ControllerUtils;
import com.wokere.easy_book.entity.UserCredentialEntity;
import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.util.Mapper;
import java.util.Deque;
import java.util.Map;
import java.util.UUID;

public class UserController implements IUserController{
    private static final IUserManager manager = SingletonServiceFactory.getBean(IUserManager.class);
    private static final IAuthenticationManager authenticationManager = SingletonServiceFactory.getBean(IAuthenticationManager.class);


    @Override
    public String createUser(InputUser user) throws ApiException {
        ControllerUtils.validateUserCreation(user);
        UUID id = manager.createUser(user);
        return Mapper.toJson(manager.getUser(id));
    }

    @Override
    public String updateUser(InputUser user, UUID userId) throws ApiException {
        ControllerUtils.validateUser(userId);
        manager.updateUser(user,userId);
        return Mapper.toJson(manager.getUser(userId));
    }

    @Override
    public String getUser(UUID id) throws ApiException {
        return Mapper.toJson(manager.getUser(id));
    }

    @Override
    public void deleteUser(UUID id) throws ApiException {
        manager.deleteUser(id);
    }

    @Override
    public String getUsers(Map<String, Deque<String>> queryParameters) throws ApiException {
        String nameFilter = ControllerUtils.getNameFilter(queryParameters);
        return Mapper.toJson(manager.getUsers(nameFilter));
    }

    @Override
    public String authenticateUser(Map<String, Object> bodyMap) throws ApiException {
        UserCredentialEntity user = Config.getInstance().getMapper().convertValue(bodyMap, UserCredentialEntity.class);
        return Mapper.toJson(authenticationManager.authenticateUser(user.dni(), user.password()));
    }
}
