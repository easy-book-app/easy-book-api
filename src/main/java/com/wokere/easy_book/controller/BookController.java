package com.wokere.easy_book.controller;

import com.networknt.exception.ApiException;
import com.networknt.service.SingletonServiceFactory;
import com.wokere.easy_book.business.IBookManager;
import com.wokere.easy_book.controller.util.ControllerUtils;
import com.wokere.easy_book.model.BookConfig;
import com.wokere.easy_book.model.DateFilter;
import com.wokere.easy_book.model.InputBook;
import com.wokere.easy_book.util.Mapper;
import java.util.Deque;
import java.util.Map;
import java.util.UUID;

public class BookController implements IBookController{
    private static final IBookManager manager = SingletonServiceFactory.getBean(IBookManager.class);

    @Override
    public String createBook(InputBook book, UUID userId) throws ApiException {
        ControllerUtils.validateUser(userId);
        int bookId = manager.createBook(book, userId);
        return Mapper.toJson(manager.getBook(bookId));
    }

    @Override
    public String updateBook(InputBook book, UUID userId, int bookId) throws ApiException {
        ControllerUtils.validateUser(userId);
        manager.updateBook(book, userId, bookId);
        return Mapper.toJson(manager.getBook(bookId));
    }


    @Override
    public void deleteBook(int id, UUID userId) throws ApiException {
        ControllerUtils.validateUser(userId);
        manager.deleteBook(id, userId);
    }

    @Override
    public String getBooks(Map<String, Deque<String>> queryParameters, UUID userId) throws ApiException {
        ControllerUtils.validateUser(userId);
        DateFilter filter = ControllerUtils.getDateFilter(queryParameters);
        return Mapper.toJson(manager.getBooks(filter, userId));
    }

    @Override
    public String getAllUserBooks(Map<String, Deque<String>> queryParameters)  {
        DateFilter filter = ControllerUtils.getDateFilter(queryParameters);
        return Mapper.toJson(manager.getBooks(filter));
    }

    @Override
    public String getBookedDates(Map<String, Deque<String>> queryParameters)  {
        DateFilter filter = ControllerUtils.getDateFilter(queryParameters);
        return Mapper.toJson(manager.getBookedDates(filter));
    }

    @Override
    public String getBookConfig() {
        return Mapper.toJson(manager.getBookConfig());
    }

    @Override
    public String updateBookConfig(BookConfig config) {
        manager.updateBookConfig(config);
        return Mapper.toJson(manager.getBookConfig());
    }
}
