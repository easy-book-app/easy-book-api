package com.wokere.easy_book.entity;

public record DynamoConfig(String arn, String key, String secret,String userTable,String region) {

}
