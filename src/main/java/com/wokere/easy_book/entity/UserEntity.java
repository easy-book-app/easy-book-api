package com.wokere.easy_book.entity;

import com.wokere.easy_book.model.InputUser;
import com.wokere.easy_book.model.User.RoleEnum;
import java.util.UUID;

public record UserEntity(String name, String surname, String dni, String email, RoleEnum role, UUID id, String password) {

    public UserEntity (InputUser iUser, UUID id, RoleEnum role, String password) {
        this(iUser.getName(), iUser.getSurname(), iUser.getDni(), iUser.getEmail(), role, id, password);
    }

    public UserEntity (InputUser iUser, UUID id, RoleEnum role) {
        this(iUser.getName(), iUser.getSurname(), iUser.getDni(), iUser.getEmail(), role, id, null);
    }
}
