package com.wokere.easy_book.entity;

import java.util.UUID;

public record UserCredentialEntity (UUID uuid, String dni, String password){
    public UserCredentialEntity(String dni, String password){
        this(null, dni, password);
    }
}
