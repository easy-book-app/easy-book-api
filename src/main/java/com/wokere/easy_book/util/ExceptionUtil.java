package com.wokere.easy_book.util;

import com.networknt.exception.ApiException;
import com.networknt.http.HttpStatus;
import com.networknt.status.Status;

public class ExceptionUtil {
    private ExceptionUtil() {
    }

    public static ApiException throwApiException(String message, HttpStatus statusCode) {
        Status status = new Status();
        status.setStatusCode(statusCode.value());
        status.setMessage(message);
        return new ApiException(status);
    }

}
