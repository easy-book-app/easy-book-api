package com.wokere.easy_book.util;

import com.networknt.config.Config;
import com.wokere.easy_book.entity.CalendarConfig;
import com.wokere.easy_book.entity.DynamoConfig;
import com.wokere.easy_book.model.JwtConfig;

public class YamlUtils {
    private YamlUtils() {
    }
    public static DynamoConfig getDynamoConfig() {
        return  (DynamoConfig) Config.getInstance()
            .getDefaultJsonObjectConfig("dynamo", DynamoConfig.class);
    }

    public static CalendarConfig getcalendarConfig() {
        return  (CalendarConfig) Config.getInstance()
            .getDefaultJsonObjectConfig("calendar", CalendarConfig.class);
    }

    public static JwtConfig getJwtConfig() {
        return  (JwtConfig) Config.getInstance()
            .getDefaultJsonObjectConfig("jwt", JwtConfig.class);
    }

}
