package com.wokere.easy_book.util;

import com.networknt.http.HttpStatus;
import com.networknt.http.MediaType;
import com.networknt.utility.StringUtils;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class SendUtils {
    private SendUtils() {
    }

    public static void send(HttpServerExchange exchange, String content, HttpStatus status) {
        if(!StringUtils.isBlank(content) && content.length() >= 5) {
            exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            exchange.setStatusCode(status.value());
        } else {
            exchange.setStatusCode(HttpStatus.NO_CONTENT.value());
        }
        exchange.getResponseSender().send(content);
    }

    public static void send(HttpServerExchange exchange) {
        exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        exchange.setStatusCode(HttpStatus.NO_CONTENT.value());
        exchange.getResponseSender().send(StringUtils.EMPTY);
    }
}
