package com.wokere.easy_book.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Mapper {
    private Mapper() {
    }

    private static final ObjectMapper MAPPER_INSTANCE = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger("com.wokere.easy_book.util.Mapper");

    static {
        MAPPER_INSTANCE.setSerializationInclusion(Include.NON_NULL);
        MAPPER_INSTANCE.registerModules(new JavaTimeModule());
        MAPPER_INSTANCE.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    public static String toJson(Object object) {
        try {
            return MAPPER_INSTANCE.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error converting the object to JSON", e);
            return null;
        }
    }
}
